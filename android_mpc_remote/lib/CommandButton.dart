import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CommandButton extends StatelessWidget {
  CommandButton(
      {Key key,
      @required this.commandID,
      @required this.fullCurrentHost,
      this.child})
      : super(key: key);

  final int commandID;
  final String fullCurrentHost;
  final Widget child;

  void _doCommand() {
    final url = '$fullCurrentHost/command.html';
    http.post(url, body: 'wm_command=$commandID').catchError((e) => print(e));
      
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(5.0),
        child: RaisedButton(
          onPressed: _doCommand,
          child: child,
        ));
  }
}
