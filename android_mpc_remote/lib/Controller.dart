import 'dart:async';

import 'package:flutter/material.dart';

import 'settings.dart';
import 'CommandButton.dart';
import 'PlayState.dart';
import 'Scrubber.dart';
import 'Volume.dart';
import 'Browser.dart';

class Controller extends StatefulWidget {
  Controller({Key key, @required this.settings}) : super(key: key);

  final Settings settings;
  final playstate = new PlayState();

  @override
  ControllerState createState() => new ControllerState();
}

class ControllerState extends State<Controller> {
  @override
  void initState() {
    super.initState();
    reloadPlayState();
  }

  void reloadPlayState() async {
    if (widget.settings.currentHostValid) {
      await widget.playstate.getState(widget.settings.fullCurrentHost());
      setState(() {});
    }
    Future.delayed(Duration(seconds: 1), reloadPlayState);
  }

  void openBrowserPage() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => BrowserPage(settings: widget.settings)));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(top: 10.0),
        child: Column(children: <Widget>[
          Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                widget.playstate.file,
                textScaleFactor: 1.5,
                textAlign: TextAlign.center,
              )),
          new PreviewImage(fullCurrentHost: widget.settings.fullCurrentHost()),
          Scrubber(
            playstate: widget.playstate,
            fullCurrentHost: widget.settings.fullCurrentHost(),
          ),
          MainButtonRow(
              fullCurrentHost: widget.settings.fullCurrentHost(),
              playState: widget.playstate.state),
          Volume(
              playstate: widget.playstate,
              fullCurrentHost: widget.settings.fullCurrentHost()),
          Row(children: [
            Spacer(flex: 1),
            RaisedButton(
                onPressed: openBrowserPage,
                child: Row(children: [
                  Padding(
                      padding: EdgeInsets.only(right: 10.0),
                      child: Icon(Icons.folder_open)),
                  Text('File Browser')
                ])),
            Spacer(flex: 1)
          ])
        ]));
  }
}

class MainButtonRow extends StatelessWidget {
  MainButtonRow(
      {Key key, @required this.fullCurrentHost, @required this.playState})
      : super(key: key);

  final String fullCurrentHost;
  final int playState;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(children: [
      Row(children: <Widget>[
        const Spacer(flex: 1),
        new CommandButton(
            child: Icon(Icons.skip_previous),
            commandID: 921,
            fullCurrentHost: fullCurrentHost),
        new CommandButton(
            child: Icon(playState == 2 ? Icons.pause : Icons.play_arrow),
            commandID: 889,
            fullCurrentHost: fullCurrentHost),
        new CommandButton(
            child: Icon(Icons.skip_next),
            commandID: 922,
            fullCurrentHost: fullCurrentHost),
        const Spacer(flex: 1)
      ]),
      Row(children: <Widget>[
        new CommandButton(
            child: Row(children: [Icon(Icons.arrow_left), Text('5s')]),
            commandID: 899,
            fullCurrentHost: fullCurrentHost),
        new CommandButton(
            child: Row(children: [Icon(Icons.arrow_right), Text('5s')]),
            commandID: 900,
            fullCurrentHost: fullCurrentHost),
        Spacer(flex: 1),
        new CommandButton(
            child: Icon(Icons.fullscreen),
            commandID: 830,
            fullCurrentHost: fullCurrentHost)
      ]),
    ]));
  }
}

class PreviewImage extends StatelessWidget {
  PreviewImage({Key key, @required this.fullCurrentHost}) : super(key: key);

  final String fullCurrentHost;

  @override
  Widget build(BuildContext context) {
    final secondsToReload = 15;
    final i =
        (new DateTime.now().millisecondsSinceEpoch / 1000 / secondsToReload)
            .round();
    if (fullCurrentHost != null) {
      return new Image.network(
        '$fullCurrentHost/snapshot.jpg?$i',
        gaplessPlayback: true,
      );
    }
    return Container();
  }
}
