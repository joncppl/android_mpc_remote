import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'PlayState.dart';
import 'CommandButton.dart';

class Volume extends StatefulWidget {
  Volume({Key key, @required this.playstate, @required this.fullCurrentHost})
      : super(key: key);

  final PlayState playstate;
  final String fullCurrentHost;

  @override
  VolumeState createState() => new VolumeState();
}

class VolumeState extends State<Volume> {
  double localVolume = 0.0;
  bool useLocalVolume = false;

  void doneScrub(volume) {
    final url = '${widget.fullCurrentHost}/command.html';
    final body = 'wm_command=-2&volume=${volume.round()}';
    http.post(url, body: body).catchError((e) => print(e)).then((repsonse) {
      widget.playstate
          .getState(widget.fullCurrentHost)
          .then((a) => setState(() {
                useLocalVolume = false;
              }));
    });
  }

  void handleChange(volume) {
    setState(() {
      localVolume = volume;
      useLocalVolume = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(
          child: Slider(
              min: 0.0,
              max: 100.0,
              value:
                  (useLocalVolume ? localVolume : widget.playstate.volumeLevel)
                      .toDouble(),
              onChanged: handleChange,
              onChangeEnd: doneScrub)),
      CommandButton(
          fullCurrentHost: widget.fullCurrentHost,
          commandID: 909,
          child: Icon(widget.playstate.muted ? Icons.volume_off : Icons.volume_up))
    ]);
  }
}
