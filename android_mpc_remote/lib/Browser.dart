import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:html/parser.dart' show parse;

import 'settings.dart';

class Browser extends StatefulWidget {
  Browser({Key key, @required this.fullCurrentHost}) : super(key: key);

  final String fullCurrentHost;

  @override
  BrowerState createState() => new BrowerState();
}

class BrowserLink {
  BrowserLink({this.browserPath, this.displayName});

  final String browserPath;
  final String displayName;
}

class BrowerState extends State<Browser> {
  @override
  void initState() {
    super.initState();
    loadBrowserPaths().then((_) => setState(() {}));
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);
    Browser oldBrowser = oldWidget;
    if (widget.fullCurrentHost != oldBrowser.fullCurrentHost) {
      loadBrowserPaths().then((_) => setState(() {}));
    }
  }

  String browserPath = '/browser.html';
  List<BrowserLink> paths = [];

  String _fullBrowserPath() {
    return '${widget.fullCurrentHost}$browserPath';
  }

  Future loadBrowserPaths() async {
    try {
      final url = _fullBrowserPath();
      final result = await http.get(url);
      final rawHTML = result.body;
      final html = parse(rawHTML);
      final browserTable = html.getElementsByClassName('browser-table')[1];
      paths = [];
      browserTable.getElementsByTagName('tr').forEach((tr) {
        final links = tr.getElementsByTagName('a');
        if (links.length < 1) {
          return;
        }
        final link = links[0];
        final browserPath = link.attributes['href'];
        var displayName = link.innerHtml;
        if (displayName == '..') {
          displayName = '.. [Up One Directory]';
        }
        paths.add(BrowserLink(
            browserPath: browserPath, displayName: displayName));
      });
    } catch (e) {
      print(e);
    }
  }

  void _pressLink(BrowserLink bl) {
    setState(() {
      browserPath = bl.browserPath;
      loadBrowserPaths().then((_) => setState(() {}));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView(
      children: paths
          .map((bl) => FlatButton(
              onPressed: () => _pressLink(bl), child: Text(bl.displayName)))
          .toList(),
    ));
  }
}

class BrowserPage extends StatelessWidget {
  BrowserPage({Key key, @required this.settings}) : super(key: key);

  final Settings settings;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('File Browser')),
        body: Browser(fullCurrentHost: settings.fullCurrentHost()));
  }
}
