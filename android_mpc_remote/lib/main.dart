import 'dart:async';

import 'package:flutter/material.dart';

import 'settings.dart';
import 'SettingsScreen.dart';
import 'Controller.dart';
import 'Browser.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Settings _settings = new Settings();

  final ThemeData theme = new ThemeData(
      primaryColor: Colors.deepPurple,
      buttonColor: Colors.deepPurple,
      buttonTheme:
          ButtonThemeData(minWidth: 36.0, textTheme: ButtonTextTheme.primary));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: theme,
        title: 'MPC Remote',
        home: MainScreen(settings: _settings));
  }
}

enum MenuEnum { settings, browser }

class MainScreen extends StatefulWidget {
  MainScreen({Key key, @required this.settings}) : super(key: key);

  final Settings settings;

  @override
  MainScreenState createState() => new MainScreenState();
}

class MainScreenState extends State<MainScreen> {
  void initState() {
    super.initState();
    widget.settings.afterCheckCurrentHost = _afterCheck;
    widget.settings.loadSettings();
  }

  void _handleMenu(MenuEnum selected, BuildContext context) {
    switch (selected) {
      case MenuEnum.settings:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    SettingsScreen(settings: widget.settings)));
        break;
      case MenuEnum.browser:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BrowserPage(settings: widget.settings)));
        break;
    }
  }

  void _afterCheck(bool isValid) {
    if (!isValid) {
      _mydialog();
    }
  }

  Future<void> _mydialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Server not found'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                new Text('Click yes to go to settings to configure it.'),
              ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('No'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            new FlatButton(
              child: new Text('Yes'),
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            SettingsScreen(settings: widget.settings)));
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('MPC Remote'), actions: <Widget>[
        new PopupMenuButton<MenuEnum>(
            onSelected: (result) => _handleMenu(result, context),
            itemBuilder: (BuildContext context) => <PopupMenuEntry<MenuEnum>>[
                  PopupMenuItem<MenuEnum>(
                      value: MenuEnum.settings,
                      child: Row(children: [
                        Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: Icon(Icons.settings)),
                        Text('Settings')
                      ])),
                  PopupMenuItem<MenuEnum>(
                      value: MenuEnum.browser,
                      child: Row(children: [
                        Padding(
                            padding: EdgeInsets.only(right: 10.0),
                            child: Icon(Icons.folder_open)),
                        Text('File Browser')
                      ]))
                ])
      ]),
      body: SingleChildScrollView(
          child: new Controller(settings: widget.settings)),
    );
  }
}
