import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'settings.dart';

class SettingsScreen extends StatefulWidget {
  SettingsScreen({Key key, @required this.settings}) : super(key: key);

  final Settings settings;

  @override
  SettingsScreenState createState() => new SettingsScreenState();
}

enum _DialogDecision { Cancel, Yes, No }

class SettingsScreenState extends State<SettingsScreen> {
  TextEditingController currentHostController;
  TextEditingController currentPortController;
  TextEditingController currentScanHostBaseController;
  TextEditingController currentScanPortController;

  void initState() {
    super.initState();
    currentHostController =
        new TextEditingController(text: widget.settings.getCurrentHost());
    currentPortController = new TextEditingController(
        text: widget.settings.getCurrentPort().toString());
    currentScanHostBaseController = new TextEditingController(
        text: widget.settings.getCurrentScanHostBase());
    currentScanPortController = new TextEditingController(
        text: widget.settings.getCurrentScanPort().toString());
  }

  void updateCurrentHost(value) {
    setState(() {
      widget.settings.setCurrentHost(value);
    });
  }

  void updateCurrentPort(value) {
    int port = int.parse(value);
    setState(() {
      widget.settings.setCurrentPort(port);
    });
  }

  void updateCurrentScanHostBase(value) {
    setState(() {
      widget.settings.setCurrentScanHostBase(value);
    });
  }

  void updateCurrentScanPort(value) {
    int port = int.parse(value);
    setState(() {
      widget.settings.setCurrentScanPort(port);
    });
  }

  void checkAddress(scaffoldContext) async {
    final valid = await widget.settings.checkCurrentHost();

    Scaffold.of(scaffoldContext).showSnackBar(new SnackBar(
      content: new Text(valid
          ? 'Found server!'
          : 'Could not find server with manually specified address.'),
    ));

    setState(() {});
  }

  void startScan(scaffoldContext) async {
    int start = 0;
    while (true) {
      start = await widget.settings.scanForPort(start: start);
      print('scanResult: $start');

      if (start > -1) {
        final dres =
            await _mydialog('${widget.settings.getCurrentScanHostBase()}$start');
        if (dres == _DialogDecision.Cancel) {
          break;
        } else if (dres == _DialogDecision.Yes) {
          Scaffold.of(scaffoldContext).showSnackBar(new SnackBar(
            content: new Text(
                'Set server to ${widget.settings.getCurrentScanHostBase()}$start!'),
          ));
          widget.settings.setCurrentHost(
              '${widget.settings.getCurrentScanHostBase()}$start');
          widget.settings.setCurrentPort(widget.settings.getCurrentScanPort());
          widget.settings.currentHostValid = true;
          break;
        } else {
          start++;
          continue;
        }
      } else {
        Scaffold.of(scaffoldContext).showSnackBar(new SnackBar(
          content: new Text('No server found.'),
        ));
        break;
      }
    }
  }

  Future<_DialogDecision> _mydialog(String url) async {
    return showDialog<_DialogDecision>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext scaffoldContext) {
        return new AlertDialog(
          title: new Text('Server found $url!'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                new Text('Use this server?'),
                new Text('If you press no the search will continue'),
              ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Cancel Scan'),
              onPressed: () {
                Navigator.of(context).pop(_DialogDecision.Cancel);
              },
            ),
            new FlatButton(
              child: new Text('No'),
              onPressed: () {
                Navigator.of(context).pop(_DialogDecision.No);
              },
            ),
            new FlatButton(
              child: new Text('Yes'),
              onPressed: () {
                Navigator.of(context).pop(_DialogDecision.Yes);
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Settings')),
        body:

        new Builder(builder: (BuildContext scaffoldContext) {
          return new GridView.count(  primary: false,
              padding: const EdgeInsets.all(20.0),
//              crossAxisSpacing: 10.0,
              crossAxisCount: 1,
              childAspectRatio: 7.0,
              children: <Widget>[
                const Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 10.0),
                    child: Text('Manually Enter Host',
                        textScaleFactor: 1.1,
                        style: TextStyle(fontWeight: FontWeight.bold))),
                new ListTile(
                    leading: const Icon(Icons.computer),
                    title: new TextField(
                        autocorrect: false,
                        maxLength: 15,
                        maxLengthEnforced: true,
                        inputFormatters: <TextInputFormatter>[
                          IPAddressFormatter()
                        ],
                        controller: currentHostController,
                        onChanged: updateCurrentHost,
                        decoration:
                            new InputDecoration(hintText: 'Host Name'))),
                new ListTile(
                  leading: const Icon(Icons.label_important),
                  title: new TextField(
                    autocorrect: false,
                    maxLength: 5,
                    maxLengthEnforced: true,
                    controller: currentPortController,
                    onChanged: updateCurrentPort,
                    decoration: new InputDecoration(hintText: 'Host Port'),
                    keyboardType: TextInputType.numberWithOptions(
                        signed: false, decimal: false),
                  ),
                ),
                new Padding(
                    padding: const EdgeInsets.only(left: 25.0, top: 20.0),
                    child: Text(
                        'http://${widget.settings.getCurrentHost()}:${widget.settings.getCurrentPort()} -- ${widget.settings.currentHostValid ? 'GOOD' : 'FAILED'}')),
                new Center(
                    child: new RaisedButton(
                        color: widget.settings.currentHostValid
                            ? Colors.lightGreen
                            : Colors.red,
                        onPressed: () => checkAddress(scaffoldContext),
                        child: Text('Check'))),
                // --- SCAN ----------------------------------------------------

                const Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 15.0),
                    child: Text('Scan For Host',
                        textScaleFactor: 1.1,
                        style: TextStyle(fontWeight: FontWeight.bold))),
                new ListTile(
                    leading: const Icon(Icons.computer),
                    title: new TextField(
                        autocorrect: false,
                        maxLength: 15,
                        maxLengthEnforced: true,
                        inputFormatters: <TextInputFormatter>[
                          IPAddressFormatter()
                        ],
                        controller: currentScanHostBaseController,
                        onChanged: updateCurrentScanHostBase,
                        decoration:
                            new InputDecoration(hintText: 'Host Name'))),
                new ListTile(
                  leading: const Icon(Icons.label_important),
                  title: new TextField(
                    autocorrect: false,
                    maxLength: 5,
                    maxLengthEnforced: true,
                    controller: currentScanPortController,
                    onChanged: updateCurrentScanPort,
                    decoration: new InputDecoration(hintText: 'Host Port'),
                    keyboardType: TextInputType.numberWithOptions(
                        signed: false, decimal: false),
                  ),
                ),
                new Padding(
                    padding: const EdgeInsets.only(left: 25.0, top: 20.0),
                    child: Text(
                        'http://${widget.settings.getCurrentScanHostBase()}.*:${widget.settings.getCurrentScanPort()}')),
                new Center(
                    child: new RaisedButton(
                        onPressed: () => startScan(scaffoldContext),
                        child: Text('Scan'))),
              ]);
        })
    );
  }
}

class IPAddressFormatter extends TextInputFormatter {
  // valid characters. digits or period
  var _regex1 = new RegExp(r'[^0-9\.]');

  // integers in groups of <= 3
  var _regex2 = new RegExp(r'([0-9]{4,})');

  // no more than 1 period
  var _regex3 = new RegExp(r'(\.{2,})');

  @override
  TextEditingValue formatEditUpdate(oldValue, newValue) {
    String toCheck = newValue.text;

    // remove all invalid characters
    toCheck = toCheck.replaceAllMapped(_regex1, (match) => '');

    // truncate groups of greater than 3 digits
    toCheck = toCheck.replaceAllMapped(
        _regex2, (match) => '${match.group(0).substring(0, 3)}');

    // truncate groups of greater than 1 period
    toCheck = toCheck.replaceAllMapped(_regex3, (match) => '.');

    // rectify selection
    var oldSelection = newValue.selection;
    var newSelection = TextSelection(
      baseOffset: oldSelection.baseOffset <= toCheck.length
          ? oldSelection.baseOffset
          : toCheck.length,
      extentOffset: oldSelection.extentOffset <= toCheck.length
          ? oldSelection.extentOffset
          : toCheck.length,
    );

    return new TextEditingValue(text: toCheck, selection: newSelection);
  }
}
