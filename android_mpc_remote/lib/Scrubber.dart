import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'PlayState.dart';

class Scrubber extends StatefulWidget {
  Scrubber({Key key, @required this.playstate, @required this.fullCurrentHost})
      : super(key: key);

  final PlayState playstate;
  final String fullCurrentHost;

  @override
  ScrubberState createState() => new ScrubberState();
}

class ScrubberState extends State<Scrubber> {
  double position = 0.0;
  bool useLocalPosition = false;

  void doneScrub(ms) {
    final a1 = (ms / 3600000).floor();
    final a2 = (ms / 60000).floor() % 60;
    final a3 = (ms / 1000).floor() % 60;
//    final a4 = (seconds) % 1000;
    final tstamp =
        '${a1 < 10 ? '0' : ''}$a1%3A${a2 < 10 ? '0' : ''}$a2%3A${a3 < 10 ? '0' : ''}$a3';
    final url = '${widget.fullCurrentHost}/command.html';
    final body = 'wm_command=-1&position=$tstamp';
    http.post(url, body: body).catchError((e) => print(e)).then((repsonse) {
      widget.playstate
          .getState(widget.fullCurrentHost)
          .then((a) => setState(() {
        useLocalPosition = false;
      }));
    });
  }

  void handleChange(ms) {
    setState(() {
      position = ms;
      useLocalPosition = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(
          child: Slider(
              min: 0.0,
              max: widget.playstate.duration.toDouble(),
              value: (useLocalPosition
                  ? position
                  : widget.playstate.position)
                  .toDouble(),
              onChanged: handleChange,
              onChangeEnd: doneScrub)),
      Padding(
          padding: const EdgeInsets.only(right: 15.0),
          child: Text(
              '${widget.playstate.positionString}/${widget.playstate.durationString}'))
    ]);
  }
}