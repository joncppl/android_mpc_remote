import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Settings {
  String _currentHost = '192.168.1.1';
  String getCurrentHost() => _currentHost;
  void setCurrentHost(String currentHost) {
    _currentHost = currentHost;
    _prefs.setString('currentHost', _currentHost);
  }

  int _currentPort = 13579;
  int getCurrentPort() => _currentPort;
  void setCurrentPort(int currentPort) {
    _currentPort = currentPort;
    _prefs.setInt('currentPort', _currentPort);
  }

  bool currentHostValid = false;

  String _currentScanHostBase = '192.168.1.';
  String getCurrentScanHostBase() => _currentScanHostBase;
  void setCurrentScanHostBase(String currentScanHostBase) {
    _currentScanHostBase = currentScanHostBase;
    _prefs.setString('currentScanHostBase', currentScanHostBase);
  }

  int _currentScanPort = 13579;
  int getCurrentScanPort() => _currentScanPort;
  void setCurrentScanPort(int currentScanPort) {
    _currentScanPort = currentScanPort;
    _prefs.setInt('currentScanPort', currentScanPort);
  }

  Function afterCheckCurrentHost;

  String fullCurrentHost() {
    // TODO: some check on _currentHost
    return 'http://$_currentHost:$_currentPort';
  }

  SharedPreferences _prefs;

  Future<Null> loadSettings() async {
    _prefs = await SharedPreferences.getInstance();
    _currentHost = _prefs.getString('currentHost') ?? _currentHost;
    _currentPort = _prefs.getInt('currentPort') ?? _currentPort;
    _currentScanHostBase = _prefs.getString('currentScanHostBase') ?? _currentScanHostBase;
    _currentScanPort = _prefs.getInt('currentScanPort') ?? _currentScanPort;
    checkCurrentHost();
  }

  String fullCurrentScan(int sub) {
    // TODO: some check on currentScanHostBase
    return 'http://$_currentScanHostBase$sub:$_currentScanPort';
  }

  Future<bool> checkCurrentHost() async {
    final address = fullCurrentHost();
    print('check $address');
    try {
      final response = await http
          .get('$address/variables.html')
          .timeout(new Duration(seconds: 1));
      currentHostValid = response.statusCode == 200;
    } catch (e) {
      currentHostValid = false;
    }
    afterCheckCurrentHost(currentHostValid);
    return currentHostValid;
  }

  bool _scanning = false;

  Future<int> scanForPort({int start = 0}) async {
    if (_scanning) {
      return -1;
    }
    _scanning = true;

    for (int i = start; i < 256; i++) {
      final address = fullCurrentScan(i);
      print('scanning: $address');
      try {
        final response = await http
            .get('$address/variables.html')
            .timeout(new Duration(milliseconds: 100));
        if (response.statusCode == 200) {
          _scanning = false;
          return i;
        }
      } catch (e) {}
    }

    _scanning = false;
    return -1;
  }
}
