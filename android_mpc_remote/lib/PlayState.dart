import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:html/parser.dart' show parse;

class PlayState {
  String file = '';
  String filePath = '';
  String fileDir = '';
  int state = 0;
  String stateString = '';
  int position = 0;
  String positionString = '';
  int duration = 0;
  String durationString = '';
  int volumeLevel = 0;
  bool muted = false;
  double playbackRate = 1.0;
  String size = '';
  String version = '';

  Future<Null> getState(String fullCurrentHost) async {
    try {
      final response = await http.get('$fullCurrentHost/variables.html')
          .timeout(const Duration(seconds: 1));
      final rawHTML = response.body;
      final parsed = parse(rawHTML);
      parsed.getElementsByTagName('p').forEach((par) {
        final id = par.id;
        final val = par.innerHtml;
        switch (id) {
          // TODO: unescape
          case 'file':
            file = val;
            break;
          case 'filepath':
            filePath = val;
            break;
          case 'filedir':
            fileDir = val;
            break;
          case 'state':
            state = int.parse(val);
            break;
          case 'statestring':
            stateString = val;
            break;
          case 'position':
            position = int.parse(val);
            break;
          case 'positionstring':
            positionString = val;
            break;
          case 'duration':
            duration = int.parse(val);
            break;
          case 'durationstring':
            durationString = val;
            break;
          case 'volumelevel':
            volumeLevel = int.parse(val);
            break;
          case 'muted':
            muted = val == '1' ? true : false;
            break;
          case 'playbackrate':
            playbackRate = double.parse(val);
            break;
          case 'size':
            size = val;
            break;
          case 'version':
            version = val;
        }
      });
    } catch (e) {
      print(e);
    }
  }
}